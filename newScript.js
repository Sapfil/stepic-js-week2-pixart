console.log('im here');
testFunc(30, 30);
paletteInit();

var mouseDown = 0;
    document.body.onmousedown = function () {
        mouseDown = 1;
    };
    document.body.onmouseup = function () {
        mouseDown = 0;
    };


function testFunc(i, j) {
    for (let jj = 0; jj < j; jj++) {
        let lineDiv = document.createElement('div');
        lineDiv.setAttribute('class','row');
        for (let ii = 0; ii < i; ii++) {
            let div = document.createElement('div');
            div.setAttribute('class', 'cell');
            div.addEventListener("mousedown", color);
            div.addEventListener("mouseenter", enterColor);
            lineDiv.appendChild(div);
        }
        document.getElementById('canvas').appendChild(lineDiv);
    }
}

function color(event) {
    // event.target.setAttribute('class', 'cellFilled')
    event.target.style.backgroundColor = window.getComputedStyle(
        document.getElementById('chosenColor'), null).backgroundColor;
}

function enterColor(event){
    if (mouseDown)
        color(event);
}

function setColor(color){
    document.getElementById('chosenColor').style.backgroundColor = color;
}

function getColor(event){
    setColor(event.target.style.backgroundColor);
}

function paletteInit(){
    var children = document.getElementById('palette').children;
    for (var i = 0; i < children.length; i++)
        children[i].addEventListener('click', getColor);
}